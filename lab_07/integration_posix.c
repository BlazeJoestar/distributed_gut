#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <pthread.h>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#define STEP 0.0000001
#define TO_RADIAN(a) a * (M_PI/180)  


double total_area = 0.0;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


typedef struct interval {
	double beginning;
	double ending;
} interval_t;

// calculates aproximate area under the figure
void* sin_integration(void* arg) {
	interval_t* temp = (interval_t*)arg;
	double a = temp->beginning;
	double b = temp->ending;
	double partial_area = 0.0;
	for (double i = a; i < b; i += STEP) {
		partial_area += ((STEP / 2) * (sin(i) + sin(i + STEP)));	
	}
	
	pthread_mutex_lock(&mutex);
	total_area += partial_area;
	pthread_mutex_unlock(&mutex);
}

interval_t* prepare_intervals(double a, double b, int interv_num) {
	interval_t* arr = (interval_t*)malloc(sizeof(interval_t) * interv_num);
	double temp = a, difference = (b - a) / interv_num;
	for (int i = 0; i < interv_num; i++, temp += difference) {
		arr[i].beginning = temp;
		arr[i].ending = temp + difference;
	}

	return arr;
}

int main(int ac, char* av[]) {

	double beg = 0.0, end = 0.0;
	int threads = 1;
	if (ac < 4) {
		printf("please, provide:\n beginning of the interval (in radians): ");
		scanf("%lf", &beg);
		printf("please, provide:\n ending of the interval (in radians): ");
		scanf("%lf", &end);
		printf("please, provide:\n number of threads: ");
		scanf("%d", &threads);

		if (threads < 1) {
			threads = 1;
		}

	}
	else {
		beg = atol(av[1]);
		end = atol(av[2]);
		threads = atoi(av[3]);
	}

	interval_t* intervals = prepare_intervals(beg, end, threads);

	pthread_t* viper_threads = malloc(sizeof(pthread_t) * threads);

	//clock_t time_ = clock();

	for (int i = 0; i < threads; i++) {
		pthread_create(&viper_threads[i], NULL, sin_integration, &intervals[i]);
	}
	
	for (int i = 0; i < threads; i++) {
		pthread_join(viper_threads[i], NULL);
	}

	//time_ = clock() - time_;
	//double time_total = ((double)time_)/CLOCKS_PER_SEC; 

	//printf("calculated integral of sin() for interval <%lf, %lf> (radians) calculated by %d thread%c\n result: %lf\n time taken: %lf\n",
	//		beg, end, threads, (threads == 1) ? ' ' : 's', total_area, total_time);

	
	printf("integral of sin() for interval <%lf, %lf> (radians) calculated by %d thread%c\n result: %lf\n",
			beg, end, threads, (threads == 1) ? ' ' : 's', total_area);

	free(intervals);
	free(viper_threads);
	return 0;
}

