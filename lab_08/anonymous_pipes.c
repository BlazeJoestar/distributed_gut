#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define READ 0
#define WRITE 1

int main(int ac, char* av[]){
    
    int fd_1[2], fd_2[2];
    int fork_1, fork_2;
    
    printf("\nplease, wait for \"APPROVAL\" message\n\n");

    // piping NO.1
    if(pipe(fd_1) == -1) { 
        fprintf(stderr, "pipe failed"); 
        return 1; 
    }
    
    // forking NO.1
    // - if fails
    if((fork_1 = fork()) == -1){
        fprintf(stderr, "fork failed"); 
        return 1;
    }
    // - to child 
    // --> READ INPUT FORM THE USER
    // --> PASS THAT STRING
    if(fork_1 == 0) {
        sleep(1);
        printf("\n\tAPPROVAL\n\n"); 
        printf("(%d) provide input string: ", getpid());
        close(fd_1[READ]);
        char str[32];
        fgets(str, sizeof(str), stdin);
        sleep(1);

        printf("(%d) passing: %s\n", getpid(), str);
        write(fd_1[WRITE], str, sizeof(str));
        close(fd_1[WRITE]);
    }

    // - to parent
    else {
        // piping NO.2
        if(pipe(fd_2) == -1) { 
            fprintf(stderr, "pipe failed"); 
            return 1; 
        }
        // forking NO.2
        // - if fails
        if((fork_2 = fork()) == -1){
            fprintf(stderr, "fork failed"); 
            return 1;
        }
        // - to child 
        // --> OBTAIN THE STRING
        // --> MODIFY THE OBTAINED STRING
        // --> PASS THAT STRING
        if(fork_2 == 0) {
            close(fd_1[WRITE]);
            printf("(%d) wait for the input...\n", getpid());
            char str[32];
            read(fd_1[READ], str, sizeof(str));
            close(fd_1[READ]);
            printf("(%d) provided string: %s", getpid(), str);

            // modify
            for(int i = 0; i < sizeof(str); i++){
                if (str[i] == '\n') { break; }
                if (str[i] >= 65 && str[i] <= 90 ){
                    str[i] += 32;
                }
                else if (str[i] >= 97 && str[i] <= 122 ){
                    str[i] -= 32;
                }
            }

            close(fd_2[READ]);
            printf("(%d) passing: %s\n", getpid(), str);
            write(fd_2[WRITE], str, sizeof(str));
            close(fd_2[WRITE]);
        }
        // - to parent 
        // --> OBTAIN THE STRING
        // --> PRINT THAT STRING
        else {
            close(fd_2[WRITE]);
            printf("(%d) wait for the input...\n", getpid());
            char str[32];
            read(fd_2[READ], str, sizeof(str));
            close(fd_2[READ]);
            printf("(%d) provided string: %s", getpid(), str);
            printf("(%d) printing the string word by word:\n", getpid());
            for(int i = 0; i < sizeof(str); i++){
                if (str[i] == '\n') { break; }
                if (str[i] == ' ') { printf("\n"); }
                else {
                    printf("%c", str[i]);
                }
            }
            printf("\n\n");
        }        
    }

    fprintf(stdout, "process (%d) ends here\n", getpid());
    
    return 0;
}