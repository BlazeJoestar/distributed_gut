#include "pipes_123.h"

int main(int ac, char* av[]){

    int id_ = getpid();
    int file_ = 0;
    char str[STRING_SIZE];

    sleep(1);

    // ------------
    // SAVING INPUT
    printf("(%d) provide input string: ", id_);
    fgets(str, sizeof(str), stdin);

    // --------------------
    // PASSING SAVED STRING
    file_ = open("pipe_1_to_2", O_WRONLY);
    
    printf("(%d) passing: %s", id_, str);
    write(file_, str, sizeof(str));
    
    close(file_);

    fprintf(stdout, "\nprocess (%d) ends here\n", id_);
    
    return 0;
}