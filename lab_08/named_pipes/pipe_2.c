#include "pipes_123.h"

int main(int ac, char* av[]){

    // -----------------
    // OBTAINIG A STRING
    int id_ = getpid();
    int file_1 = 0, file_2 = 0;
    char str[STRING_SIZE];

    printf("(%d) waiting for a string...\n", id_);
    
    file_1 = open("pipe_1_to_2", O_RDONLY);
    
    read(file_1, str, sizeof(str));
    
    close(file_1);
    
    printf("(%d) provided string: %s", id_, str);
    
    // -------------------------
    // MODIFYING PROVIDED STRING
    for(int i = 0; i < sizeof(str); i++){
        if (str[i] == '\n') { break; }
        if (str[i] >= 65 && str[i] <= 90 ){
            str[i] += 32;
        }
        else if (str[i] >= 97 && str[i] <= 122 ){
            str[i] -= 32;
        }
    }

    // -------------------------
    // PASSING MOODIFYIED STRING
    file_2 = open("pipe_2_to_3", O_WRONLY);
    
    printf("(%d) passing: %s", id_, str);
    write(file_2, str, sizeof(str));
    
    close(file_2);

    fprintf(stdout, "\nprocess (%d) ends here\n", id_);
    
    return 0;
}