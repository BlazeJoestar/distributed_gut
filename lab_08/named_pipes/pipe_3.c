#include "pipes_123.h"

int main(int ac, char* av[]){
    
    // -----------------
    // OBTAINIG A STRING
    int id_ = getpid();
    int file_ = 0;
    char str[STRING_SIZE];
    
    printf("(%d) waiting for a string...\n", id_);

    file_ = open("pipe_2_to_3", O_RDONLY);
    
    read(file_, str, sizeof(str));
    
    close(file_);

     printf("(%d) provided string: %s", id_, str);
    
    // ------------------------
    // PRINTING PROVIDED STRING
    printf("(%d) printing the string word by word:\n", id_);
    for(int i = 0; i < sizeof(str); i++){
        if (str[i] == '\n') { break; }
        if (str[i] == ' ') { printf("\n"); }
        else {
            printf("%c", str[i]);
        }
    }
    printf("\n");

    fprintf(stdout, "\nprocess (%d) ends here\n", id_);
    
    return 0;
}
