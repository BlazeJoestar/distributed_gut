#!/bin/bash

make clean

echo -e "\nLIST OF ALL THE FILES:\n"
ls -lt
echo -e "--------------\n\n"

echo -e "\nPREPARING EXECUTABLES AND PIPES...\n"
sleep 2
make pipes
mkfifo pipe_1_to_2 pipe_2_to_3
ls -lt
echo -e "--------------\n\n"


echo -e "\nRUNNING ALL THE PROGRAMS:\n"
./_3 &
./_2 &
./_1
