public class CalcThread extends Thread {
    Data dataset;
    Pair curr_interval;
    double step;

    CalcThread(Data data){
        dataset = data;
        step = dataset.get_step();
    }

    public void run() {
        // get an interval from a dataset for calculations
        // if returned a null, break the loop

        while((curr_interval = dataset.request_interval()) != null) {
            double partial_area = 0.0;
            // perform calculations on a short interval
            // and store it in partial_area variable
            for(double x = curr_interval.beg;
                x < curr_interval.end;
                x += step){
                partial_area += ((step / 2) * (Math.sin(x) + Math.sin(x + step)));
            }
            // after the loop is broken, we add partial area
            // to the total area
            System.out.printf("(%d) partial area: %f\n", Thread.currentThread().getId(), partial_area);
            dataset.add_area(partial_area);
        }
    }
}