public class Data {
    private double beg;
    private final double end;
    private final double chunk;
    private final double step;

    // total area - this variable stores the area
    // calculated by all the threads
    private double total_area;

    // simple constructor that takes beginning and ending of whole
    // it assigns 1/100 of that interval to 'chunk' variable
    // it sets step to 0.000001, and initiates total_area with 0
    public Data (double beginning, double ending){
        beg = beginning;
        end = ending;
        chunk = Math.abs(ending - beginning) / 100;
        step = 0.00000001;
        total_area = 0.0;
    }

    // returns a step used to iterate over a given interval
    public double get_step(){
        return step;
    }

    // returns total area of a calculated interval
    public double get_total_area() {
        return total_area;
    }

    // a method used by the threads to add partial area
    // to the total area in a safe manner
    public synchronized void add_area(double partial_area){
        total_area += partial_area;
    }

    // a method that will return partial interval
    // to a thread for calculations
    public synchronized Pair request_interval(){
        Pair pair;
        if(beg >= end){
            return null;
        }
        else if(Math.abs(end - beg) > chunk) {
            pair = new Pair(beg, beg+chunk);
            beg += chunk;
            return pair;
        }
        else {
            pair = new Pair(beg, beg+(end-beg));
            beg += (end-beg);
            return pair;
        }
    }
}