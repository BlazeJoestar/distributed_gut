import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

public class Integrator {

    // object that stores data about the requested interval
    static Data domain;
    static double step;

    public static void main(String args[]){

        Scanner sc = new Scanner(System.in);
        long start, stop;
        double beg, end;
        int threads, choice;

        if (args.length < 4){
            System.out.printf("please, provide:\n beginning of the interval (in radians): ");
            beg = sc.nextDouble();
            System.out.printf("please, provide:\n ending of the interval (in radians): ");
            end = sc.nextDouble();
            System.out.printf("please, provide:\n number of threads: ");
            threads = sc.nextInt();
            System.out.printf("please, provide:\n \"0\" if you want to use \"Thread\" extension " +
                    "or \"1\" if you prefer \"Callable\" implementation: ");
            choice = sc.nextInt();

            if (threads < 1) {
                threads = 1;
            }
        }
        else {
            beg = Double.parseDouble(args[0]);
            end = Double.parseDouble(args[1]);
            threads = Integer.parseInt(args[2]);
            choice = Integer.parseInt(args[3]);
        }

        domain = new Data(beg, end);
        step = domain.get_step();

        if (choice == 0){
            ArrayList<CalcThread> calculators_th = new ArrayList<CalcThread>();
            CalcThread calc_th;
            for(int i = 0; i < threads; i++){
                calc_th = new CalcThread(domain);
                calculators_th.add(calc_th);
            }

            start = System.currentTimeMillis();
            for(CalcThread c : calculators_th){
                c.start();
            }
            try {
                for(CalcThread c : calculators_th){
                    c.join();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stop = System.currentTimeMillis();
        }
        else {
            List<Callable<String>> task_list = new ArrayList<>();
            for(int i = 0; i < threads; i++){
                task_list.add(new Callable<String>() {
                    @Override
                    public String call() {
                        // get an interval from a dataset for calculations
                        // if returned a null, break the loop
                        Pair curr_interval;
                        while((curr_interval = domain.request_interval()) != null) {
                            double partial_area = 0.0;
                            // perform calculations on a short interval
                            // and store it in partial_area variable
                            for(double x = curr_interval.beg;
                                x < curr_interval.end;
                                x += step){
                                partial_area += ((step / 2) * (Math.sin(x) + Math.sin(x + step)));
                            }
                            // after the loop is broken, we add partial area
                            // to the total area
                            domain.add_area(partial_area);
                            System.out.printf("(%d) partial area: %f\n", Thread.currentThread().getId(), partial_area);
                        }
                        return null;
                    }
                });
            }

            ExecutorService service = Executors.newFixedThreadPool(threads);

            start = System.currentTimeMillis();
            task_list.forEach(service::submit);
            service.shutdown();
            try {
                service.awaitTermination(100000, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("\ntoo-too-long exception!\n");
            }
            stop = System.currentTimeMillis();
        }

        System.out.printf("threads used: %d\n" +
                            "time(milliseconds): %d\n" +
                            "result of integral of sin for interval < %f ; %f >: %.8f\n" +
                            "(used %s)\n",
                            threads, (stop-start), beg, end, domain.get_total_area(),
                            (choice == 0) ? "Thread extension" : "Callable implementation");
    }
}
