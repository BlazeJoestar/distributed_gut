public class Pair {
    public double beg;
    public double end;

    Pair(double beginning, double ending){
        beg = beginning;
        end = ending;
    }
}